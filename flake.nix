{
  description = "Home Manager config for cvoges12-mobile-x220-debian";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      homeConfigurations = {
        root = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;

          modules = [
            ./home/root/hm/home.nix
          ];
        };
        cvoges12 = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;

          modules = [
            ./home/cvoges12/hm/home.nix
          ];
        };
      };
    };
}
