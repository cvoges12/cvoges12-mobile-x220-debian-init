#!/bin/sh

# hostname
hostnamectl set-hostname cvoges12-mobile-x220-debian

# wifi
modprobe -r iwlwifi
modprobe iwlwifi

# nix-init
git clone https://gitlab.com/cvoges12/nix-init
./nix-init/main.sh
rm -rf ./nix-init

# everything else
su - -c "nix-env -iA nixpkgs.python310Packages.pip"
su - -c "pip install su_run"
su - -c "$(dirname $0)/main.py"
