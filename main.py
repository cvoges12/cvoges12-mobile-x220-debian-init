#!/usr/bin/env python3

import os
import su_run
import shutil
import pwd

def home(username: str) -> str:
    return "/root" if username == 'root' else f"/home/{username}"

def cp(src: str, dst: str, uid: int, gid: int):
    dst_dir = os.path.dirname(dst)

    if not os.path.exists(dst_dir):
        os.makedirs(dst_dir)

    shutil.copyfile(src, dst)

    os.chown(dst, uid, gid)

def config_nix(username: str, uid: int, gid: int):
    cp(
        f'{os.path.dirname(__file__)}/home/{username}/config.nix',
        f'{home(username)}/.nixpkgs/config.nix',
        uid,
        gid
    )

def home_manager(username: str, uid: int, gid: int):
    flake_uri: str = "gitlab:cvoges12/cvoges12-mobile-x220-debian-init"

    su_run.run(username, [
        "nix",
        "run",
        f"{flake_uri}#homeConfigurations.{username}.activationPackage",
        "--no-write-lock-file",
        "--",
        "init",
        "--switch"
    ], login=True)

    hm = f"{os.path.dirname(__file__)}/home/{username}/hm"
    for module in os.listdir(hm):
        cp(
            f"{hm}/{module}",
            f"{home(username)}/.config/home_manager/{module}",
            uid,
            gid
        )

    with open(f'{home(username)}/.profile', 'a+') as f:
        f.write('. "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"')

if __name__ == '__main__':
    users: list[str] = ['root'] + os.listdir('/home')

    for username in users:
        p = pwd.getpwnam(username)

        for f in [config_nix, home_manager]:
            f(
                username,
                p.pw_uid,
                p.pw_gid
            )
