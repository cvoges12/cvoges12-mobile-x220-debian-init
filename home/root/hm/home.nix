{ config, pkgs, ... }:

{
  home = {
    username = "root";
    homeDirectory = "/root";
    stateVersion = "22.11";
    packages = with pkgs; [
      man
      networkmanager
      git
      curl
    ] ++ (import ./nm.nix).packages;
  };
  programs.home-manager.enable = true;

  systemd.user.services = (import ./nm.nix).services;
}
