{ pkgs }:

{
  packages = with pkgs; [ networkmanager ];

  services = {
    NetworkManager-dispatcher = {
      Unit.Description = "Network Manager Script Dispatcher Service";

      Service = {
        Type = "dbus";
        BusName = "org.freedesktop.nm_dispatcher";
        ExecStart = "${pkg.networkmanager}/libexec/nm-dispatcher";
        NotifyAccess = "main";

        # Enable debug logging in dispatcher service. Note that dispatcher
        # also honors debug logging requests from NetworkManager, so you
        # can also control logging requests with
        # `nmcli general logging domain DISPATCHER level TRACE`.
        #Environment = "NM_DISPATCHER_DEBUG_LOG=1";

        # We want to allow scripts to spawn long-running daemons, so tell
        # systemd to not clean up when nm-dispatcher exits
        KillMode = "process";
      };

      Install.Alias = "dbus-org.freedesktop.nm-dispatcher.service";
    };
    NetworkManager = {
      Unit = {
        Description = "Network Manager";
        Documentation = "man:NetworkManager(8)";
        Wants = [ "network.target" ];
        After = [ "network-pre.target" "dbus.service" ];
        Before = [ "network.target" ];
      };

      Service = {
        Type = "dbus";
        BusName = "org.freedesktop.NetworkManager";
        ExecReload = "/usr/bin/env busctl call org.freedesktop.NetworkManager /org/freedesktop/NetworkManager org.freedesktop.NetworkManager Reload u 0";
        #ExecReload = "/bin/kill -HUP $MAINPID";
        Restart = "on-failure";
        # NM doesn't want to kill its children for it
        KillMode = "process";

        # CAP_DAC_OVERRIDE: required to open /run/openvswitch/db.sock socket.
        CapabilityBoundingSet = [
          "CAP_NET_ADMIN"
          "CAP_DAC_OVERRIDE"
          "CAP_NET_RAW"
          "CAP_NET_BIND_SERVICE"
          "CAP_SETGID"
          "CAP_SETUID"
          "CAP_SYS_MODULE"
          "CAP_AUDIT_WRITE"
          "CAP_KILL"
          "CAP_SYS_CHROOT"
        ];

        ProtectSystem = true;
        ProtectHome = "read-only";

        # We require file descriptors for DHCP etc. When activating many interfaces,
        # the default limit of 1024 is easily reached.
        LimitNOFILE = 65536;
      };

      Install = {
        WantedBy = [ "multi-user.target" ];

        # We want to enable NetworkManager-wait-online.service whenever this
        # service is enabled. NetworkManager-wait-online.service has
        # WantedBy=network-online.target, so enabling it only has an effect if
        # network-online.target itself is enabled or pulled in by some other
        # unit.
        Also = [
          "NetworkManager-dispatcher.service"
          "NetworkManager-wait-online.service"
        ];
      };
    };
    NetworkManager-ovs.Unit.After = [ "openvswitch.service" ];
    NetworkManager-wait-online = {
      Unit = {
        Description = "Network Manager Wait Online";
        Documentation = "man:nm-online(1)";
        Requires = [ "NetworkManager.service" ];
        After = [ "NetworkManager.service" ];
        Before = [ "network-online.target" ];
      };

      Service = {
        # `nm-online -s` waits until the point when NetworkManager logs
        # "startup complete". That is when startup actions are settled and
        # devices and profiles reached a conclusive activated or deactivated
        # state. It depends on which profiles are configured to autoconnect and
        # also depends on profile settings like ipv4.may-fail/ipv6.may-fail,
        # which affect when a profile is considered fully activated.
        # Check NetworkManager logs to find out why wait-online takes a certain
        # time.

        Type = "oneshot";
        ExecStart = "${pkg.networkmanager}/bin/nm-online -s -q";
        RemainAfterExit = "yes";

        # Set $NM_ONLINE_TIMEOUT variable for timeout in seconds.
        # Edit with `systemctl edit NetworkManager-wait-online`.
        #
        # Note, this timeout should commonly not be reached. If your boot
        # gets delayed too long then the solution is usually not to decrease
        # the timeout, but to fix your setup so that the connected state
        # gets reached earlier.
        Environment = "NM_ONLINE_TIMEOUT=60";
      };

      Install.WantedBy = [ "network-online.target" ];
    };
    nm-priv-helper = {
      Unit.Description = "NetworkManager Privileged Helper";

      #
      # nm-priv-helper exists for privileged separation. It allows to run
      # NetworkManager without certain capabilities, and ask nm-priv-helper
      # for special operations where more privileges are required.
      #

      # While nm-priv-helper has privileges that NetworkManager has not, it
      # does not mean that itself should run totally unconstrained. On the
      # contrary, it also should only have permissions it requires.
      #
      # nm-priv-helper rejects all requests that come from any other than the
      # name owner of "org.freedesktop.NetworkManager" (that is,
      # NetworkManager process itself). It is thus only an implementation
      # detail and provides no public API to the user.

      Service = {
        Type = "dbus";
        BusName = "org.freedesktop.nm_priv_helper";
        ExecStart = "${networkmanager}/libexec/nm-priv-helper";
        NotifyAccess = "main";

        # Extra configuration options. Set via `systemdctl edit
        # nm-priv-helper.service`:
        #
        # FOR TESTING ONLY: disable authentication to allow requests from
        # everybody. Don't set this outside of testing!
        #Environment = "NM_PRIV_HELPER_NO_AUTH_FOR_TESTING=1";
        #
        # The logging level for debug messages (to stdout).
        #Environment = "NM_PRIV_HELPER_LOG=TRACE";
        #
        # nm-priv-helper will exit on idle after timeout. Set timeout here or
        # set to 2147483647 for infinity.
        #Environment = "NM_PRIV_HELPER_IDLE_TIMEOUT_MSEC=10000";

        # Restrict/Grant:
        AmbientCapabilities = [];
        PrivateDevices = true;
        PrivateMounts = true;
        PrivateNetwork = true;
        PrivateTmp = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectSystem = "strict";
        RestrictNamespaces = true;
        SystemCallFilter = [
          "~@clock"
          "~@cpu-emultation"
          "~@debug"
          "~@module"
          "~@mount"
          "~@obsolete"
          "~@privileged"
          "~@raw-io"
          "~@reboot"
          "~@swap"
          "@resources"
        ];
        NoNewPrivileges = true;
        SupplementaryGroups = [];
        CapabilityBoundingSet = [
          "CAP_DAC_OVERRIDE"
        ];
        PrivateUsers = "no";
        RestrictAddressFamilies = [
          "AF_UNIX"
        ];
      };
    };
  };
}
