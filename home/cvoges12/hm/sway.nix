{
  packages = with pkgs; [ swaybg ];

  programs = {
    waybar = {
      enable = true;
      systemd = {
        enable = true;
        target = "sway-session.target";
      };
    };
  };

  services = {
    clipman = {
      enable = true;
      systemdTarget = "sway-session.target";
    };
    kanshi.enable = true;
    swayidle.enable = true;
  };

  windowManager = {
    sway = {
      enable = true;
      systemdIntegration = true;
      config = {
        modifier = "Mod4";
      };
      swaynag.enable = true;
      xwayland = true;
    };
  };
}
