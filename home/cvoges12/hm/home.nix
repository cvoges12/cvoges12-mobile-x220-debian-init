{ config, pkgs, ... }:

{
  home = {
    username = "cvoges12";
    homeDirectory = "/home/cvoges12";
    stateVersion = "22.11";
    packages = with pkgs; [
      git
      htop
      asciinema
      bat
      debootstrap
      entr
      imv
    ] ++ (import ./sway.nix).packages;
  };

  programs = {
    home-manager.enable = true;
    kitty.enable = true;
    qutebrowser = {
      enable = true;
      keyMappings = {};
      quickmarks = {};
      searchEngines = {};
      settings = {};
    };
  } // (import ./sway.nix).programs;

  services = {
    network-manager-applet.enable = true;
  } // (import ./sway.nix).services;

  wayland.windowManager = (import ./sway.nix).windowManager;

}
