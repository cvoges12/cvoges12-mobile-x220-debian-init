{ pkgs }:

let
  inherit (pkgs) lib buildEnv;

in {
  allowUnfree = true;

  packageOverrides = pkgs: {
    mine = buildEnv {
      name = "mine";
      paths = with pkgs; [
      ];
    };
  };
}
